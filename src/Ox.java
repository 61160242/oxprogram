/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
import java.util.*;

public class Ox {
     static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
     static Scanner kb = new Scanner(System.in);
     static String win;
     static char player = 'X';
     static String welcome;
    public static void main(String[] args) {
        int col = 0;
        int row = 0;
        int count = 0;
        
        
        showWelcome(welcome);
        showTable(table);

        while (true) {
            showTurn();
            input(row, col);
            count++;

            if (checkDraw(count)) {
                break;
            }
            showTable(table);
            if (checkWin(win)) {
                showWin();
                break;
            }

            switchTurn(player);
        }
    }

    public static void showWelcome(String welcome) {
        System.out.println("Welcome to OX Game");
    }

    public static void showTable(char table[][]) {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void showTurn() {
        System.out.println("Turn  " + player);
    }

    public static void input(int row, int col) {
        try {
            System.out.print("Please input Row Col : ");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = player;
            } else {
                showWrong();
                kb.nextLine();
                showTurn();
                input(row, col);
            }
        } catch (Exception e) {
                showWrong();
                kb.nextLine();
                showTurn();
                input(row, col);
        }
    }

    public static void showWin() {
        System.out.println("Player " + player + " win..");
    }

    public static void showBye() {
        System.out.println("Bye Bye...");
    }

    public static boolean checkWin(String win) {
        return checkWinRow(player, 0)
                || checkWinRow(player, 1)
                || checkWinRow(player, 2)
                || checkWinCol(player, 0)
                || checkWinCol(player, 1)
                || checkWinCol(player, 2)
                || checkCross1(player)
                || checkCross2(player);
    }

    public static boolean checkWinRow(char player, int row) {
        return table[row][0] == player
                && table[row][1] == player
                && table[row][2] == player;
    }

    public static boolean checkWinCol(char player, int col) {
        return table[0][col] == player
                && table[1][col] == player
                && table[2][col] == player;
    }

    public static boolean checkCross1(char player) {
        return table[0][0] == player
                && table[1][1] == player
                && table[2][2] == player;
    }

    public static boolean checkCross2(char player) {
        return table[0][2] == player
                && table[1][1] == player
                && table[2][0] == player;
    }

    public static boolean checkDraw(int count) {
        if (count == 9 && !checkWin(win)) {
            showDraw();
            return true;
        }
        return false;
    }

    public static void switchTurn(char p) {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    public static void showWrong() {
        System.out.println("Please input position again");
    }

    public static void showDraw(){
        System.out.println("Draw!!");
    }

}
